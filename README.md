# The Concept

Give Drupal site managers a reasonably convenient(*) way to generate tabular composite data on arbitrary entities, to answer questions such as 

• What’s the mix of gender, age and nationality declared by users in their profiles, and how has it evolved over time?

• What patterns and trends exist in a site's user-generated content?

• Which units in an organization are, or have been, most active in managing content, broken down by content type and category?

(*) In a perfect world, a builder-style UI, à la Views.




# Background

This idea arose in working with an upstart NGO that had attracted a lot of engagement with its Drupal website and wanted to be able to report user statistics and trends to support fundraising. 

The organization had a small, overstretched staff and very limited resources, and were hoping for a solution they could implement themselves with no out of pocket expense.  As they expected new reporting categories to emerge over time, and as calculations would have to be re-run frequently in order to provide up to date information, automating as much of the process as possible was an important objective.

Their site manager was reasonably adept in Drupal, and had been trying to create a solution using Views, but hadn't been able to make it work. They'd also considered a manual process (exporting the data, working with it in a spreadsheet, then posting the results), but were concerned they wouldn't be able to handle it.  Training was an issue, but the bigger problem was that they simply had no one who had the time to do the work.

After mulling things over and scanning the field, I concluded they were essentially out of luck.  No system exists that, without significant implementation costs, can dynamically compile a report covering multiple, complex composite statistics on Drupal entities.  Though free data analysis applications exist, they wouldn't provide an automated solution without expensive integration work.  The only Drupal-based option is Views, but it doesn't support their use-case.  Views can extract the data, and (with some significant caveats) compute aggregate values, but it can't handle aggregation along multiple axes as was required and as is usually required in reporting scenarios.  Solutions could be created that used Views, but they would involve either significant manual processing effort at reporting time, or would be expensive to implement, probably inflexible, and almost certainly difficult to maintain.

As a Drupal developer, and all-around tender-hearted guy, I was disappointed not to be able to point these good people to a shiny module that solved their problems.  Especially because such a thing could certainly be created.  Which triggered a though.  Maybe the thing this planet of ours really needs is a "Content Analysis and Reporting" Drupal contrib module...
 

# Contraindications

Unfortunately, this is quite possibly a ridiculously bad idea.  

Report building systems are notoriously unwieldy and complex, for reasons that aren’t going to be different in this case, and would likely be exacerbated by the need to operate within the Drupal framework.  There are good reasons to question whether this concept is realistic given the effort it would require.  

There are also good reasons to question the potential value of such a system even if the effort required to build it were modest.  Cases like that of the NGO, where the data would be materially useful but resource constraints are severe, are probably relatively uncommon.  Even in those situations it’s not clear whether such a system would represent an improvement over existing options.  Given the overhead of installing and learning to use a complex module, it may offer no great advantage over more manual, ad hoc solutions.  


# Hope, In its Eternal Springiness

Despite the many reasons for circumspection, there may be potential here.

While there may not be a huge felt need for a tool like this, it's possible that the existence of one would reveal latent demand, and release latent value.  Information about the information in a site could have a great number of significant uses for site operators.  Even in organizations where resource constraints are not a barrier, and even in those which already perform analytics on their Drupal-managed content, a tool provided within Drupal itself could add value by enabling wider and more diverse engagement with the information.

Also, the concerns about the difficulty of implementing such a system may not be insurmountable.  Even if the idea of building a feature-rich reporting tool within Drupal is wildly impractical, there may be other, more efficient ways to skin this cat.  For instance:

• Instead of a builder-style UI, a bundle of canned, hard-coded reports covering the most widely applicable and highest-value scenarios, with a framework to allow community members to contribute additional reports and extend/modify existing ones, and an API to support user-configurable report options.

• Instead of a completely Drupal-based solution, a bridge/adapter that integrates a 3rd-party, open source system with Drupal entity data.  


# To Do

* Clean up provisional architecture and UI sketches.  (This is cart-before-horse-ish, but may serve as useful discussion fodder.)

* Compile and post write-up and supporting materials

* Float proposal and solicit feedback from technical and domain experts

Questions: 

** Would such a system be of significant value for a significant number of people? 

** Would it create _new_ value, or are there existing alternatives that cover the same bases?  

** Is the concept feasible from an "ROI" perspective?  

** Is it feasible in the context of a community-based development model?  

** If multiple options look promising, which would it make the most sense to pursue considering execution concerns, net value gain, time to "market"?
